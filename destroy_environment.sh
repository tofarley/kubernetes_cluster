#!/usr/bin/env bash

NODE_IDS=$(doctl compute droplet list --tag-name kubernetes_cluster --no-header --format "ID")
for id in ${NODE_IDS} ; do
  doctl compute droplet delete ${id} --force
done

# TODO: make this a variable!
rm -f cluster_PRODUCTION.*
rm -f kube_config_cluster_PRODUCTION.yml
