# INSTALL.md

## Install the local requirements
Install  onto your local system.
We have provided Linux distribution-agnostic script to install the following requirements:
* doctl   v1.36.0
* kubectl v1.16.2
* rke     v1.0.4
* helm    v3.0.3
* rancher v2.3.2 
* mc      v2020-01-25
```
./local_install.sh
```
* TODO: Error checking in local_install script.

## Build cluster nodes on digitalocean and create an ansible inventory file.
The following script will build four CentOS 7 virtual machines on DigitalOcean which will be tagged `kubernetes_cluster` and generate an ansible `inventory.ini` file containing the public IP addresses of the three newly-created machines. Finally, it will update the DNS record for `kube.homeskillet.org` to match the IP address of `kube-n01`, which will serve as our control plane.

Additionally, this script will use the file `cluster_template.yml` to establish build our cluster. The script uses `sed` as a quick-n-dirty jinja template, to replace the variables `{{public_ip}}` and `{{private_ip}}` with the values returned from `doctl`.
```
./build_environment.sh
```
* TODO: Parameterize the DNS record.
* TODO: Parameterize the name. PRODUCTION is hard-coded.

## Install docker on all nodes via ansible
This is very comprehensive playbook. It does more than most other docker install playbooks I've seen on the web, including kernel module management.
```
ansible-playbook -i inventory.ini install_docker.yml
```

## Build the cluster with RKE and install rancher
This will generate a file called `cluster_PRODUCTION.yml` that rke can use to build our cluster:
```
rke up --config cluster_PRODUCTION.yml
```

This will generate 2 files in the local directory:
* kube_config_cluster_PRODUCTION.yml: The Kubeconfig file for the cluster, this file contains credentials for full access to the cluster.
* cluster_PRODUCTION.rkestate: The Kubernetes Cluster State file, this file contains credentials for full access to the cluster.

At this point you should have four digitalocean CentOS 7 virtual machines named `kube-master` and `kube-n0[1..3]` respectively. The nodes are configured as a kubernetes cluster. `kube-n01` is your control plane.

Connect to your cluster with `kubectl` by exporting the `$KUBECONFIG` variable:
```
mkdir -p ~/.kube
cp kube_config_cluster_PRODUCTION.yml ~/.kube/
export KUBECONFIG=$HOME/.kube/kube_config_cluster_PRODUCTION.yml
echo 'export KUBECONFIG=$HOME/.kube/kube_config_cluster_PRODUCTION.yml' >> ~/.bashrc
```

Verify that your cluster is responsive with `kubectl get nodes` and `kubectl cluster-info`. The IP address of your control plane should match the public ip of `kube-n01`.

## Add the rancher repository
```
helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
helm repo update
```

## Create the cattle-system namespace
```
kubectl create namespace cattle-system
```

## Install rancher
We will install rancher and set `ingress.tls.source=secret` so that we can provide our own TLS certificates.
```
helm install rancher rancher-latest/rancher \
  --namespace cattle-system \
  --set hostname=kube.homeskillet.org \
  --set ingress.tls.source=secret
```

## Generate a TLS certificate
Generate a TLS certificate for our domain.
```
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -subj "/C=US/ST=Texas/L=SAT/O=GITI/OU=dreamerlabs/CN=kube.homeskillet.org/emailAddress=admin@homeskillet.org" -out tls.crt -keyout tls.key
```

## Install our certificates for rancher ingress
```
kubectl -n cattle-system create secret tls tls-rancher-ingress \
  --cert=tls.crt \
  --key=tls.key
```

## Install MinIO
MinIO provides our distributed storage.

Generate a random `MINIO_ACCESS_KEY` and `MINIO_SECRET_KEY` and the corresponding configs.
```
./generate_config.sh
```

Install MinIO and goofys onto the nodes. goofys allows us to fuse-mount minio as a directory.
```
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventory.ini install_golang.yml
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventory.ini install_minio.yml
```

## Destroying the environment
Destroy all DigitalOcean VMs tagged `kubernetes_cluster` using `doctl`.
```
./destroy_environment.sh
```
