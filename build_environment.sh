#!/usr/bin/env bash

CLUSTER_NAME="PRODUCTION"
RANCHER_DOMAIN=${RANCHER_DOMAIN-homeskillet.org}
DO_RECORD_ID=${DO_RECORD_ID-89006212}

# Verify that doctl is configured for use.
doctl account get 1> /dev/null
if [[ $? != 0 ]]; then
  echo "This script relies on the digitalocean cli (doctl)"
  echo "Please install and configure doctl."
  echo "https://github.com/digitalocean/doctl#installing-doctl"
  exit 1
fi

# Check that the user has exported an SSH key fingerprint.
if [ -z ${DO_SSH_KEY+x} ]; then
  echo "Please set the environment variable DO_SSH_KEY to a public"
  echo "key fingerprint that has been uploaded to DigitalOcean."
  echo "https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/to-account/"
  exit 1
fi

# Ensure that there are no existing nodes tagged for our cluster.
if [[ $(doctl compute droplet list --format "PublicIPv4" --no-header --tag-name kubernetes_cluster | wc -l) -gt 0 ]] ; then
  echo "Existing nodes found with the tag kubernetes_cluster."
  echo "Delete them before continuing"
  exit 1
fi

doctl compute tag create kubernetes_cluster 1> /dev/null

echo "Building digitalocean virtual machines..."
for node in kube-master kube-n01 kube-n02 kube-n03; do
	doctl compute droplet create ${node} \
		--size s-2vcpu-4gb \
		--image centos-7-x64 \
		--region nyc1 \
		--ssh-keys ${DO_SSH_KEY} \
		--enable-private-networking \
		--tag-name "kubernetes_cluster" \
                --format "Name,PublicIPv4,PrivateIPv4" \
		--wait
done

PUBLIC_IPS=$(doctl compute droplet list --format "PublicIPv4" --no-header --tag-name kubernetes_cluster | tac)
PRIVATE_IPS=$(doctl compute droplet list --tag-name kubernetes_cluster --no-header --format "PrivateIPv4" | tac)

CONTROL_PLANE=$(echo "${PUBLIC_IPS}" | head -1)

# Update the DNS record for kube.homeskillet.org
echo "Updating ${RANCHER_DOMAIN} DNS record to ${CONTROL_PLANE}"
doctl compute domain records update ${RANCHER_DOMAIN} --record-id=${DO_RECORD_ID} --record-data="${CONTROL_PLANE}"
if [[ $? -ne 0 ]]; then
  echo "Failed to update DNS record for ${RANCHER_DOMAIN} ..."
  echo "- I'm guessing you're not Tim. Set the environment variables"
  echo "- RANCHER_DOMAIN and DO_RECORD_ID, or just update DNS yourself..."
  echo "- This is not a showstopping error!"
fi

echo "Creating ansible inventory.ini"
# Create an ansible inventory file with our new servers.
cat > inventory.ini <<EOL
[nodes]
$(echo "${PUBLIC_IPS}")

[nodes:vars]
ansible_connection=ssh
ansible_ssh_user=root
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
EOL

echo "Verifying that there are exactly 4 nodes for our cluster..."
if [[ $(echo "${PUBLIC_IPS}" | wc -l) -ne 4 ]] ; then
  "There are not exactly four nodes tagged for this cluster. exiting."
  exit 1
fi

cp cluster_template.yml cluster_${CLUSTER_NAME}.yml

echo "Writing cluster_${CLUSTER_NAME}.yml"

for ip in ${PUBLIC_IPS}; do
  sed -i -e "0,/{{public_ip}}/ s/{{public_ip}}/${ip}/" cluster_${CLUSTER_NAME}.yml
done

for ip in ${PRIVATE_IPS}; do
  sed -i -e "0,/{{private_ip}}/ s/{{private_ip}}/${ip}/" cluster_${CLUSTER_NAME}.yml
done
