#!/usr/bin/env bash

CLUSTER_NAME=${CLUSTER_NAME:-PRODUCTION}
BASE_NAME=${BASE_NAME:-kube}
IMAGE_ID=${IMAGE_ID:-2429d1c9-ff5f-4943-a663-38170191b1b2}
EX_NET_ID=${EX_NET_ID:-6c928965-47ea-463f-acc8-6d4a152e9745}
IN_NET_ID=${IN_NET_ID:-8ba4f9aa-032a-4a33-83f5-d9bea5a03218}
SSH_KEY=${SSH_KEY:-timxps}
FLAVOR=${FLAVOR:-s1-8}
VOLUME_SIZE=${VOLUME_SIZE:-50}

declare -a PUBLIC_IPS=()
declare -a PRIVATE_IPS=()

for i in 1 2 3 4 ; do
  SERVER_NAME=${BASE_NAME}-0${i}
  echo "Creating ${SERVER_NAME}..."
  VOL_ID=$(openstack volume create -f value -c id --size ${VOLUME_SIZE} --type classic ${BASE_NAME}-vol${i})
  INSTANCE_ID=$(openstack server create -f value -c id --wait \
                          --nic net-id=${EX_NET_ID} \
                          --nic net-id=${IN_NET_ID} \
                          --flavor ${FLAVOR} \
                          --image ${IMAGE_ID} \
                          --key-name ${SSH_KEY} \
                          --block-device-mapping sdb=${VOL_ID} \
                          ${SERVER_NAME})
  
  OS_ADDRESSES=$(openstack server show ${INSTANCE_ID} -f value -c addresses)
  PUBLIC_IP=$(echo $OS_ADDRESSES | cut -f 2 -d '=' | cut -f 1 -d ',')
  PRIVATE_IP=$(echo $OS_ADDRESSES | cut -f 3 -d '=')
  PUBLIC_IPS+=("${PUBLIC_IP}")
  PRIVATE_IPS+=("$PRIVATE_IP")
done

echo "Infrastrure completed. The formatted string below can be used for installing ceph via ceph-deploy."
echo "export CEPH_NODES=${_servers}" > nodes.sh
echo "export CEPH_IPS=${_ips}" >> nodes.sh
echo "Source the file nodes.sh to initialize the variables CEPH_NODES and CEPH_IPS"

# Create an ansible inventory file with our new servers.
cat > inventory.ini <<EOL
[nodes]
$(printf '%s\n' "${PUBLIC_IPS[@]}")

[nodes:vars]
ansible_connection=ssh
ansible_ssh_user=centos
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
EOL


cp cluster_template.yml cluster_${CLUSTER_NAME}.yml

echo "Writing cluster_${CLUSTER_NAME}.yml"

for ip in ${PUBLIC_IPS[@]}; do
  sed -i -e "0,/{{public_ip}}/ s/{{public_ip}}/${ip}/" cluster_${CLUSTER_NAME}.yml
done

for ip in ${PRIVATE_IPS[@]}; do
  sed -i -e "0,/{{private_ip}}/ s/{{private_ip}}/${ip}/" cluster_${CLUSTER_NAME}.yml
done


echo "Complete. Remember to update your DNS!"

